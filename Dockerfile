FROM ubuntu:18.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
    && apt-get install -y gnupg supervisor software-properties-common debconf-utils \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv 7BD9BF62 \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv 0xF1656F24C74CD1D8 \
    && echo 'deb http://nginx.org/packages/mainline/ubuntu/ bionic nginx' > /etc/apt/sources.list.d/nginx.list \
    && echo 'deb [arch=amd64,i386,ppc64el] http://mirror.klaus-uwe.me/mariadb/repo/10.4/ubuntu bionic main' > /etc/apt/sources.list.d/mariadb.list \
    && add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y nginx imagemagick php7.3-fpm php7.3-cli php7.3-mysqlnd php7.3-curl php7.3-gd php-imagick php7.3-intl php7.3-json php7.3-mysql php7.3-readline php7.3-xml mariadb-server nginx \
    && rm -r /etc/nginx \
    && mkdir -p /var/www /run/php
COPY . /
RUN service mysqld start; sleep 5; mysql -u root -e "CREATE USER 'admin'@'%' IDENTIFIED BY 'pass';";mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' WITH GRANT OPTION;" \
    && sed -i 's/bind-address/#bind-address/' /etc/mysql/my.cnf \
    && chown -R www-data:www-data /var/www /run/php \
    && apt-get clean
EXPOSE 80 3306
CMD ["supervisord"]



